package com.ncmcomputing;

/**
 * Created by Nilton on 4/7/2017.
 *
 *
 */
public class User {
    private final String name;
    private final long id;

    public User(String name, long id) {
        this.name = name;
        this.id = id;
    }

    public static User of(String name) {
        return new User(name, name.length());
    }
    
    @Override
    public String toString() {
        return "name: " + name;
    }
    
    /**
    * Usesul comment!!
    */
    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != user.id) return false;
        return name != null ? name.equals(user.name) : user.name == null;
    }
}
