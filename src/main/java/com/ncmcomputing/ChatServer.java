package com.ncmcomputing;

/**
 * Created by Nilton on 4/7/2017.
 *
 * This is the main class that implements a very simple chat server that supports the following messages:
 * The list of configured users (along with their passwords) is maintained in a 'users.cfg' text file.
 * This file also contains a list of user groups.
 * User groups enable users to chat with all (active) members of that group, that is it enables a user to send and
 * receive chat messages from any other active member of that group.
 *
 * The basic workflow for a user is as follows:
 *  1) send Login msg
 *  2) upon successful login, send listUsers msg
 *  3)
 *
 * The following messages are currently supported:
 *
 *      Note: All messages contain a header which uniquely identifies it, that is all messages contain a unique ID.
 *      simply a numeric number
 *
 *      -Heartbeat (0):
 *          d: regular messages sent between a client and the server to detect connectivity issues
 *          note: configured with timeout (default is 25s)
 *      - Login (1):
 *          d: used to login a configured user
 *          p:     userName, pwd
 *      - LoginResponse (2):
 *          c: response [accept/reject], session?
 *      - Logout (3):
 *          p:     userName
 *      - ListUsers (4):
 *          d: a message that requests a list of ALL the users (and groups) that are currently configured in the system
 *          regardless of whether they're friends of the sender, or share a group with the sender. This message enables
 *          a user to send a message to an existing friend (or group). See Main.java for a proposed enhancement of
 *          this message.
 *
 *          p: none: default behavior
 *             activeOnly: returns only the list of currently logged in users. User groups aren't applicable here.
 *      - ListUsersResponse (5):
 *          d: returns the universe of users and userGroups. Note that this list isn't necessarily the list of
 *          currently logged in users. It dependes on the ListUsers (A) message argument.
 *          c: userList: <comma separated list of userNames>, e.g. u: 'nilton', 'cassandra', 'melanie'
 *             (optional) groups: <comma separated list of userGroups>, e.g.  g: 'family', 'workers'
 *      - ListUserGroups (6):
 *          d: a message that is used to list the contents of a user group.
 *          p: userGroup(s)
 *          ex: 'family', 'workers'
 *      - ListUserGroupsResponse (7):
 *          c: groups: <comma separated list of userGroups contents>, e.g.  g: 'family' {'}, 'workers'
 *          limitation/note: a group can't contain other groups
 *      - Chat (A):
 *          d: a message that contains ascii text data targeted to a user or to a userGroup
 *          p: from-user, to-user/to-userGroup, text
 *          ex c: from: 'nilton', to: 'cassandra', 'hello!'
 *          ex c: from: 'nilton', to: 'family', 'hello world!'
 *          note/limitation: binary data is not currently supported, so no pics, videos, etc
 *      - JoinGroup (B):
 *          d: a message used to enable a user to (automatically) become a member of a given userGroup.
 *          p: u: [user], g: ['comma separated list of groups the user wants to join']
 *          c ex: u: nilton, g: 'family', 'familiar proximo'
 *      - JoinGroupResponse (C):
 *         c: response [accept/reject]
 *      - LeaveGroup (D):
 *          d: a message used to enable a user to leave a given userGroup.
 *          p: u: [user], g: ['a group the user is currently a member of']
 *          c ex: u: nilton, g: 'family'
 *          note: to simplify the message processing, only a single group can be sent per message.
 *      - LeaveGroupResponse (E):
 *         c: response [accept/reject]
 *
 */
public class ChatServer {
}
