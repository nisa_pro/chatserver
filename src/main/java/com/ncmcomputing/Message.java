/**
 * Created by Nilton on 4/7/2017.
 */
package com.ncmcomputing;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Nilton
 * @since 1.8
 */
public interface Message {
    static AtomicLong ID_GENERATOR = new AtomicLong();
    static long newId() {
        return ID_GENERATOR.incrementAndGet();
    }
    static Message of(byte[] m) {
        return null;
    }

    long id();
    User getSender();
    Optional<Set<User>> getRecipient();
}
