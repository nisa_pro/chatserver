package com.ncmcomputing;

/**
 * Created by Nilton on 4/7/2017.
 *
 * Nice to have features (not currently supported):
 *
 * 1) add 'FriendshipRequest' message:  this enables a user to become 'friends' with the other user or 'friends' with
 * all the members on a user group if the FriendShipRequest is targetted at a user group instead of a single user.
 *
 * 2) parameterize the 'ListUsers' message to handle a large population of users. At the moment this message just
 * returns ALL the available users in the system thereby enabling anyone to send friendship requests to anyone else
 * in the system. This obviously becomes less and less useful as the number of users in the system grows. So parameters
 * should be added to this message, such as: age range, gender, occupation, interests, etc.
 *
 * 3) Enhance the 'ListUserGroupsResponse (7)' message with support for groups of groups.
 *
 * 4) Enhance the 'ChatMessage (A)' message to support binary data payload (e.g. photos, videos, etc)
 *
 * 5) Add the concept of circle of friends. Note: this is different than the concept of user groups. User groups is
 * just a simple user set used to facilitate a kind of 'multicast' communication method. Circle of friends are a
 * private club that prevent 'outsiders' of the club to communicate with club members. In this mode, you'd have to
 * request private club membership (to say a private club admin or, simply, to any private club member) prior to
 * initiate contact with any of those members. This concept can be applied further to require any user to first
 * become firends with any other user prior to being able to communicate with a user.
 *
 * 5) Add group membership and circle of friends persistence.
 *
 * 6) add UI
 *
 */
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        logger.info("{}", args[0]);
//        logger.debug("Temperature set to {}. Old temperature was {}.", t, oldT);
    }
}
