package com.ncmcomputing;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Created by Nilton on 4/14/2017.
 */
public class LoginMessage implements Message {

    private final long id;
    private final User sender;
    private final Set<User> recipient;

    public LoginMessage(String sender, String recipient) {
        this.id = Message.ID_GENERATOR.incrementAndGet();
        this.sender = User.of(sender);
        this.recipient = new HashSet<User>();
        this.recipient.add(User.of(recipient));
    }

    @Override
    public long id() {
        return id;
    }

    @Override
    public User getSender() {
        return sender;
    }

    @Override
    public Optional<Set<User>> getRecipient() {
        return Optional.of(recipient);
    }
}
