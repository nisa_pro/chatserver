package com.ncmcomputing;

/**
 * Created by Nilton on 4/17/2017.
 */
public enum MessageTags {

    MSG_TYPE_TAG(35),
    MSG_ID_TAG(1),     // long
    SENDER_ID_TAG(2),   // string identifying the user id
    RECIPIENT_ID_TAG(3),
    HB_DURATION_IN_MSECS_TAG(40),
    TEXT_TAG(50);

    private int tagId;

    MessageTags(int tagId) {
        this.tagId = tagId;
    }

    public int tagId() {
        return tagId;
    }

    public static enum MSG_TYPES {
        HEARTBEAT(0),
        LOGIN(1),
        LOGOUT(2);

        private int tagId;

        MSG_TYPE(int tagId) {
            this.tagId = tagId;
        }

        public int tagId() {
            return tagId;
        }
    }
}
